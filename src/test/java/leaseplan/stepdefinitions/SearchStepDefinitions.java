package leaseplan.stepdefinitions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import leaseplan.actions.SearchActions;
import leaseplan.model.Product;
import leaseplan.utils.BuildURI;
import net.serenitybdd.rest.SerenityRest;
import org.junit.Assert;
import java.util.List;

public class SearchStepDefinitions {
    private SearchActions searchActions = new SearchActions();
    private List<Product> productList;
    private Integer statuscode;

    private int statusCode;

    @Given("user searched an available product (.*)$")
    public void userLooksForAvailableProduct(String product) {
        statuscode = searchActions.productSearch(product);
        productList = searchActions.mapResponse();
    }

    @When("user looks out for an unavailable product (.*)$")
    public void userLooksForUnavailableProduct(String product) {
        statuscode = searchActions.productSearch(product);
    }

    @Given("user can search for a product$")
    public void userCanSearchForProduct() {
        Integer okStatus = 200;
        statuscode = searchActions.productSearch("cola");
        Assert.assertEquals(okStatus, statuscode);
    }

    @When("user with method Post calls product (.*)$")
    public void userMakesPostWithProduct(String product) {
        statuscode = searchActions.makePost(product);
    }

    @When("user checks if the result is displayed only for (.*)$")
    public void userSeesExpectedResult(String product) {
        Assert.assertTrue(searchActions.checkResponseIsAsExpected(productList, product));
    }

    @Then("the price is in between (.*) and (.*)$")
    public void checkPriceInterval(Double lowPrice, Double highPrice) {
        Assert.assertTrue(searchActions.checkPriceIsInRange(productList, lowPrice, highPrice));
    }

    @Then("user get will get a {int} error")
    public void checkHTTPError(Integer errorCode) {
        Assert.assertEquals(errorCode, statuscode);
    }
}
