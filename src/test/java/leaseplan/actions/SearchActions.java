package leaseplan.actions;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.response.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import leaseplan.model.Product;
import leaseplan.utils.BuildURI;
import net.serenitybdd.rest.SerenityRest;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toSet;

public class SearchActions {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchActions.class);
    private final ObjectMapper objectMapper = new ObjectMapper();
    private String jsonResponse;

    public Integer productSearch(String product) {
        LOGGER.info("Searching for product " + product);
        Response response = SerenityRest.given().log().uri().spec(BuildURI.searchReqSpec()).pathParam("product", product).get("v1/search/demo/{product}");
        jsonResponse = response.getBody().asString();
        response.getStatusCode();
        return response.getStatusCode();
    }

    public Integer makePost(String product) {
        LOGGER.info("Making POST for " + product);
        Response response = SerenityRest.given().log().uri().spec(BuildURI.searchReqSpec()).pathParam("product", product).post("v1/search/demo/{product}");
        jsonResponse = response.getBody().asString();
        response.getStatusCode();
        return response.getStatusCode();
    }

    public List<Product> mapResponse() {
        LOGGER.warn("Mapping response...");
        List<Product> productList;
        try {
            productList = objectMapper.readValue(jsonResponse, new TypeReference<List<Product>>(){});
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
        return productList;
    }

    public boolean checkResponseIsAsExpected(List<Product> productList, String product) {
        List<String> productTitles =
                productList.stream()
                        .map(Product::getTitle)
                        .collect(Collectors.toList());

        if (product.equalsIgnoreCase("cola")) {
            List<String> expectedProducts = Arrays.asList("Cola", "Coca-Cola", "Blik", "Pepsi", "cola", "Coca-cola");
            return isProductExpected(productTitles, expectedProducts);
        }
        if  (product.equalsIgnoreCase("pasta")) {
            List<String> expectedProducts = Arrays.asList("penne", "Rigatoni", "Pasta", "rigate", "Tagliatelle", "pesto", "sfogliata", "tortellini", "spaghetti");
            return isProductExpected(productTitles, expectedProducts);
        }
        LOGGER.warn("Other products have been returned as well");
        return false;
    }

    public boolean checkPriceIsInRange(List<Product> productList, Double lowPrice, Double highPrice) {
        LOGGER.info("Checking if price is between " + lowPrice + " and " + highPrice);
         return productList.stream()
                .filter(product -> !isInRange(product.getPrice(), lowPrice, highPrice))
                .collect(Collectors.toList()).isEmpty();
    }

    private boolean isInRange (Double price, Double lowPrice, Double highPrice) {
        return (lowPrice <= price && price <= highPrice);
    }

    private boolean isProductExpected(List<String> actualProducts, List<String> expectedProducts) {
        for (String expectedProduct : expectedProducts) {
            actualProducts.removeIf(s -> s.toLowerCase().contains(expectedProduct.toLowerCase()));
        }
        return actualProducts.isEmpty();
    }

}
