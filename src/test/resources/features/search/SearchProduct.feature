@smoke @regression
Feature: Search for the product

  Scenario Outline: Search for an available product and check that price is in expected range
    Given user searched an available product <product>
    When user checks if the result is displayed only for <product>
    Then the price is in between <lowPrice> and <highPrice>

    Examples:
      | product | lowPrice | highPrice |
      | cola    | 0.10     | 100.00     |
      | pasta   | 0.10     | 100.00     |

  @negative
  Scenario: Search for a product that does not exist
    Given user can search for a product
    When user looks out for an unavailable product rice
    Then user get will get a 404 error

  Scenario: Verify post method is not allowed
    Given user can search for a product
    When user with method Post calls product cola
    Then user get will get a 405 error
