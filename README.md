# LeasePlan-Example

LeasePlan is a Java Test Automation Project that uses Maven and Serenity BDD to run automated tests written in a Gherkin format.

### What is Serenity BDD?

Serenity BDD is a test automation library designed to make writing automated acceptance tests easier by using plain text under a Gherkin Format.

```Gherkin
Scenario Outline: Search for an available product and check that price is in expected range
  When user looks out for an available product <product>
  Then user sees the results displayed only for <product>
  And the price is in between <lowPrice> and <highPrice>

Examples:
| product | lowPrice | highPrice |
| cola    | 0.57     | 10.46     |
| pasta   | 0.10     | 10.00     |
```
### Where does Java come into place?

Each of the above steps is implemented using Java code to achieve it's intended scope.

```java
    @When("user looks out for an available product (.*)$")
    public void userLooksForAvailableProduct(String product) {
        statuscode = searchActions.productSearch(product);
        productList = searchActions.mapResponse();
    }

    @Then("user sees the results displayed only for (.*)$")
    public void userSeesExpectedResult(String product) {
        Assert.assertTrue(searchActions.checkResponseIsAsExpected(productList, product));
        }

    @Then("the price is in between (.*) and (.*)$")
    public void checkPriceInterval(Double lowPrice, Double highPrice) {
        Assert.assertTrue(searchActions.checkPriceIsInRange(productList, lowPrice, highPrice));
        }
```

New tests will need to follow the same format, first of all the Gherkin will need to be properly written with a Test Setup Step (@Given), an Action Step (@When) and an Assertion Step (@Then).

Each of the steps needs to be implemented using Java code unless an exiting Method already exists for the step in which case it can be re-used.


## Running the Project

### Locally

#### Prerequisites 

 - Git
 - Java 11 or higher
 - Maven

The project needs to be cloned locally from the remote repository on Gitlab with the following command:

```json
git clone https://gitlab.com/test5544847/leaseplan.git
```   

Import the project in a Java IDE of your choice

Running the following command will run the tests on the QA environment (available environments are DEV, QA, Staging and Production)

```json
$ mvn clean verify -Denvironment=qa
```


### In Gitlab CI/CD pipeline

The pipeline running the tests can be found here: https://gitlab.com/test5544847/leaseplan/-/pipelines/1082675261/

A Serenity report will be generated with the status for each individual test
https://gitlab.com/test5544847/leaseplan/-/jobs/$JOB_NUMBER/artifacts/external_file/target/site/serenity/index.html

# Refactoring

 - Folders were renamed accordingly with the Project name
 - The Feature file was renamed to properly reflect the functionality covered
 - The Configuration file for Serenity was modified with required variables to be able to run the tests on multiple environments
 - A Utility Package was created to store Utils classes (In this case the BuildUri.java which is used to pass environments variables to the build command)
 - A Model Package was created to map Json Response Body to Java Objects which will make Assertion easier to write and understand
 - CarsAPI.java was removed as it had no purpose for the automation tests
 - SearchStepDefinitions was refactored to only use methods defined in a separate Actions package, this will make reading and understanding the scope of Scenarios easier  
 - An Action Package was created to store the actual Java Classes and Methods that will be used by the StepDefinition classes that implement the Gherkin Steps

Feature file was altered to cover more scenarios and functionalities, a Scenario Outline was added to verify multiple products, a negative scenario was added to verify an unavailable products and last, a scenario was added to check 405 errors. 

    
